# Test Cases

**Newly_created_stack:**

- Given newly created stack
- When getting Count of items
- Then Count should be 0

**Empty_stack:**

- Given an empty stack
- When getting Count of items
- Then Count should be 0

**Poping_an_item_from_a_stack_that_have_at_least_one_item_in_it:**

- Given a stack with two items
- When Poping an item from the stack
- Then the returned item should be the last item pushed onto the stack
- And Count now should be 1

**Poping_an_item_from_an_empty_stack:**

- Given an empty stack
- When Poping an item from the stack
- Then EmptyStackException is thrown
- And expected error message returned

**Peeking_an_item_from_a_stack_that_have_at_least_one_item_in_it:**

- Given a stack with two items
- When Peeking the top item
- Then item returned should be the last item pushed onto the stack
- And Count should still be 2

**Peeking_an_item_from_an_empty_stack:**

- Given an empty stack
- When Peeking the top item
- Then EmptyStackException is thrown
- And error messages matchs

**Pushing_an_item_on_to_a_stack_that_is_not_full:**

- Given a stack with two items
- When Pushing an item on to the stack
- Then the newly added item is placed on top of the stack
- And Count now should be 3

**Pushing_an_item_on_to_a_full_stack:**

- Given a full stack
- When Pushing an item on to the stack
- Then FullStackException is thrown

**Clearing_a_stack_with_items:**

- Given a stack with items
- When Clearing the stack
- Then Count should now be 0

**Clearing_an_empty_stack:**

- Given an empty stack
- When Clearing the stack
- Then Count should still be 0

**Contains_should_return_true_for_existing_item:**

- Given a stack with items
- When calling Contains for existing item
- Then [true] should be returned

**Contains_should_return_false_for_non_existing_item:**

- Given a stack with items
- When calling Contains for non-existing item
- Then [false] should be returned

**Contains_should_return_false_when_stack_is_empty:**

- Given an empty stack
- When calling Contains
- Then [false] should be returned
