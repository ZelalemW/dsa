# Queue test cases

**Newly_created_queue_should_be_empty:**

- Given newly created queue
- When getting Count of items
- Then Count should be 0
- And IsEmpty should be true

**Empty_queue:**
  
- Given newly an empty queue
- When getting Count of items
- Then Count should be 0
- And IsEmpty should be true

**Enqueue_an_item_to_the_queue:**

- Given a stack with some items
- When Equeue an item to the queue
- Then the Last item item in the queue should be {itemToEnqueue}
- And Count now should be {existingQueueItems.Length + 1}

**Dequeue_an_item_from_an_empty_queue:**

- Given an empty queue
- When Dequeuing an item from the queue
- Then EmptyQueueException is thrown

**Last_returns_last_item_added_to_the_queue:**

- Given a queue with three items:
- When Last is called
- Then last item added to the queue is returned.
- And Count should still be 3

**First_returns_first_item_added_to_the_queue:**

- Given a queue with three items
- When First is called"
- Then first item added to the queue is returned.
- And Count should still be 3

**Last_throws_exception_when_queue_is_empty:**

- Given an empty queue
- When Last is called"
- Then EmptyQueueException is thrown

**First_throws_exception_when_queue_is_empty:**

- Given an empty queue
- When Last is called
- Then EmptyQueueException is thrown

**void Clearing_a_queue_with_items:**

- Given a queue with items
- When Clearing the queue
- Then Count should now be 0

**Clearing_an_empty_queue:**

- Given an empty queue
- When Clearing the queue
- Then Count should still be 0

**Contains_should_return_true_for_existing_item:**

- Given a queue with items
- When calling Contains for existing item
- Then [true] should be returned

**Contains_should_return_false_for_non_existing_item:**

- Given a queue with items
- When calling Contains for non-existing item
- Then [false] should be returned