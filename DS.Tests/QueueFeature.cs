using DS.Exceptions;
using System;
using Xbehave;
using Xunit;

namespace DS.Tests
{
    public class QueueFeature : IDisposable
    {
        private DS.Queue<int> _queue;

        public QueueFeature()
        {
            // setup the test here. This will be run just before every test method runs
            _queue = new DS.Queue<int>();
        }

        public void Dispose()
        {
            // put your clean up here.
            // This will run after each test run to make sure, any state change introduced because of the current test run SHOULDN'T affect the next test.
            _queue = null;
        }

        [Fact]
        public void Newly_created_queue_should_be_empty()
        {
            int result = 0;
            "Given newly created queue"
                .x(() => _queue = new DS.Queue<int>());
            "When getting Count of items"
                .x(() => result = _queue.Count);
            "Then Count should be 0"
                .x(() => Assert.Equal(0, result));
            "And IsEmpty should be true"
                .x(() => Assert.True(_queue.IsEmpty));
        }


        [Fact]
        public void Empty_queue()
        {
            int result = 0;
            "Given newly an empty queue"
                .x(() => _queue.Clear());
            "When getting Count of items"
                .x(() => result = _queue.Count);
            "Then Count should be 0"
                .x(() => Assert.Equal(0, result));
            "And IsEmpty should be true"
                .x(() => Assert.True(_queue.IsEmpty));
        }


        [Fact]
        public void Enqueue_an_item_to_the_queue()
        {
            var existingQueueItems = new int[] { 1, 2, 3, 4, 5 };
            int itemToEnqueue = 100;

            "Given a stack with some items"
                .x(() =>
                {
                    _queue.Clear();

                    for (int i = 0; i < existingQueueItems.Length; i++)
                    {
                        _queue.Enqueue(existingQueueItems[i]);
                    }
                });
            "When Equeue an item to the queue"
                .x(() => _queue.Enqueue(itemToEnqueue));
            $"Then the Last item item in the queue should be {itemToEnqueue}"
                .x(() => Assert.Equal(itemToEnqueue, _queue.Last));
            $"And Count now should be {existingQueueItems.Length + 1}"
              .x(() => Assert.Equal(existingQueueItems.Length + 1, _queue.Count));
        }
        

        [Fact]
        public void Dequeue_an_item_from_an_empty_queue()
        {
            Exception ex = null;

            "Given an empty queue"
                .x(() =>
                {
                    _queue.Clear();
                });
            "When Dequeuing an item from the queue"
                .x(() => ex = Assert.Throws<EmptyQueueException>(() => _queue.Dequeue()));
            "Then EmptyQueueException is thrown"
                .x(() => Assert.Equal(Constants.QueueIsEmpty, ex.Message));
        }

        [Fact]
        public void Last_returns_last_item_added_to_the_queue()
        {
            int lastItemFromQueue = 0;
            int lastItemAddedToTheQUeue = 300;

            "Given a queue with three items"
                .x(() =>
                {
                    _queue.Clear();
                    _queue.Enqueue(100);
                    _queue.Enqueue(200);
                    _queue.Enqueue(lastItemAddedToTheQUeue);
                });
            "When Last is called"
             .x(() => lastItemFromQueue = _queue.Last);
            "Then last item added to the queue is returned."
                .x(() => Assert.Equal(lastItemAddedToTheQUeue, lastItemFromQueue));
            "And Count should still be 3"
              .x(() => Assert.Equal(3, _queue.Count));
        }

        [Fact]
        public void First_returns_first_item_added_to_the_queue()
        {
            int firstItemAddedToTheQueue = 300;
            int firstItemFromQueue = 0;

            "Given a queue with three items"
                .x(() =>
                {
                    _queue.Clear();
                    _queue.Enqueue(firstItemAddedToTheQueue);
                    _queue.Enqueue(firstItemAddedToTheQueue + 1);
                    _queue.Enqueue(firstItemAddedToTheQueue + 3);
                });
            "When First is called"
             .x(() => firstItemFromQueue = _queue.First);
            "Then first item added to the queue is returned."
                .x(() => Assert.Equal(firstItemAddedToTheQueue, firstItemFromQueue));
            "And Count should still be 3"
              .x(() => Assert.Equal(3, _queue.Count));
        }

        [Fact]
        public void Last_throws_exception_when_queue_is_empty()
        {
            Exception ex = null;
            "Given an empty queue"
                .x(() =>
                {
                    _queue.Clear();
                });
            "When Last is called"
             .x(() => ex = Assert.Throws<EmptyQueueException>(()=>_queue.Last));
            "Then EmptyQueueException is thrown"
                .x(() => Assert.Equal(Constants.QueueIsEmpty, ex.Message));
        }

        [Fact]
        public void First_throws_exception_when_queue_is_empty()
        {
            Exception ex = null;
            "Given an empty queue"
                .x(() =>
                {
                    _queue.Clear();
                });
            "When Last is called"
             .x(() => ex = Assert.Throws<EmptyQueueException>(()=> _queue.First));
            "Then EmptyQueueException is thrown"
                .x(() => Assert.Equal(Constants.QueueIsEmpty, ex.Message));
        }

        [Fact]
        public void Clearing_a_queue_with_items()
        {
            "Given a queue with items"
                .x(() =>
                {
                    _queue.Clear(); // wipe out anything that someone might have pushed on to the stack before this line.

                    for (int i = 1; i <= 10; i++)
                    {
                        _queue.Enqueue(i);
                    }
                });
            "When Clearing the queue"
                .x(() => _queue.Clear());
            "Then Count should now be 0"
                .x(() => Assert.Equal(0, _queue.Count));
        }

        [Fact]
        public void Clearing_an_empty_queue()
        {
            "Given an empty queue"
                .x(() =>
                {
                    _queue.Clear(); // wipe out anything that someone might have pushed on to the stack before this line.
                });
            "When Clearing the queue"
                .x(() => _queue.Clear());
            "Then Count should still be 0"
                .x(() => Assert.Equal(0, _queue.Count));
        }


        [Fact]
        public void Contains_should_return_true_for_existing_item()
        {
            var values = new int[] { 1, 3, 5, 6, 9 };
            bool containsResult = false;

            "Given a queue with items"
                .x(() =>
                {
                    _queue.Clear();

                    for (int i = 0; i < values.Length; i++)
                    {
                        _queue.Enqueue(values[i]);
                    }
                });
            "When calling Contains for existing item"
                .x(() =>
                {
                    // any of the values push onto the stack should return true
                    int randIndex = new Random().Next(0, 4);
                    containsResult = _queue.Contains(values[randIndex]);
                });
            "Then [true] should be returned"
                .x(() => Assert.True(containsResult));
        }

        [Fact]
        public void Contains_should_return_false_for_non_existing_item()
        {
            var values = new int[] { 1, 3, 5, 6, 9 };
            bool containsResult = false;
            int nonExistingItem = 100;

            "Given a queue with items"
                .x(() =>
                {
                    _queue.Clear(); // wipe out anything that someone might have pushed on to the stack before this line.

                    for (int i = 0; i < values.Length; i++)
                    {
                        _queue.Enqueue(values[i]);
                    }
                });
            "When calling Contains for non-existing item"
                .x(() =>
                {
                    containsResult = _queue.Contains(nonExistingItem);
                });
            "Then [false] should be returned"
                .x(() => Assert.False(containsResult));
        }
    }
}
