using DS.Exceptions;
using System;
using Xbehave;
using Xunit;

namespace DS.Tests
{
    public class StackFeature : IDisposable
    {
        private int MAX_SIZE = 1000;
        private DS.Stack<int> _stack;

        public StackFeature()
        {
            // setup the test here. This will be run just before every test method runs
            _stack = new DS.Stack<int>();
        }

        public void Dispose()
        {
            // put your clean up here.
            // This will run after each test run to make sure, any state change introduced because of the current test run SHOULDN'T affect the next test.
            _stack = null;
        }

        [Fact]
        public void Newly_created_stack()
        {
            int result = 0;
            "Given newly created stack"
                .x(() => _stack = new DS.Stack<int>());
            "When getting Count of items"
                .x(() => result = _stack.Count);
            "Then Count should be 0"
                .x(() => Assert.Equal(0, result));
        }

        [Fact]
        public void Empty_stack()
        {
            int result = 0;
            "Given an empty stack"
                .x(() => _stack.Clear()); // make sure stack is empty
            "When getting Count of items"
                .x(() => result = _stack.Count);
            "Then Count should be 0"
                .x(() => Assert.Equal(0, result));
        }

        [Fact]
        public void Poping_an_item_from_a_stack_that_have_at_least_one_item_in_it()
        {
            int topItem = 0;
            "Given a stack with two items"
                .x(() =>
                {
                    _stack.Clear();//first clear it and push two items
                    _stack.Push(1);
                    _stack.Push(190);
                });
            "When Poping an item from the stack"
                .x(() => topItem = _stack.Pop());
            "Then the returned item should be the last item pushed onto the stack"
                .x(() => Assert.Equal(190, topItem));
            "And Count now should be 1"
              .x(() => Assert.Equal(1, _stack.Count));
        }

        [Fact]
        public void Poping_an_item_from_an_empty_stack()
        {
            Exception ex = null;

            "Given an empty stack"
                .x(() =>
                {
                    _stack.Clear(); // first make sure the stack has nothing in it.
                });
            "When Poping an item from the stack"
                .x(() => ex = Assert.Throws<EmptyStackException>(() => _stack.Pop()));
            "Then EmptyStackException is thrown"
                .x(() => Assert.Equal(Constants.StackIsEmpty, ex.Message));
        }

        [Fact]
        public void Peeking_an_item_from_a_stack_that_have_at_least_one_item_in_it()
        {
            int topItem = 0;
            "Given a stack with two items"
                .x(() =>
                {
                    _stack.Clear(); // first make sure the stack has nothing in it.
                    _stack.Push(1);
                    _stack.Push(190);
                });
            "When Peeking the top item"
             .x(() => topItem = _stack.Peek());
            "Then item returned should be the last item pushed onto the stack"
                .x(() => Assert.Equal(190, topItem));
            "And Count should still be 2"
              .x(() => Assert.Equal(2, _stack.Count));
        }

        [Fact]
        public void Peeking_an_item_from_an_empty_stack()
        {
            Exception ex = null;

            "Given an empty stack"
                .x(() =>
                {
                    _stack.Clear(); // first make sure the stack has nothing in it.
                });
            "When Peeking the top item"
                .x(() => ex = Assert.Throws<EmptyStackException>(() => _stack.Peek()));
            "Then EmptyStackException is thrown"
                .x(() => Assert.Equal(Constants.StackIsEmpty, ex.Message));
        }

        [Fact]
        public void Pushing_an_item_on_to_a_stack_that_is_not_full()
        {
            "Given a stack with two items"
                .x(() =>
                {
                    _stack.Clear();// first make sure the stack has nothing in it.
                    _stack.Push(100);
                    _stack.Push(200);
                });
            "When Pushing an item on to the stack"
                .x(() => _stack.Push(300));
            "Then the newly added item is placed on top of the stack"
                .x(() => Assert.Equal(300, _stack.Peek()));
            "And Count now should be 3"
              .x(() => Assert.Equal(3, _stack.Count));
        }

        [Fact]
        public void Pushing_an_item_on_to_a_full_stack()
        {
            Exception ex = null;

            "Given a full stack"
                .x(() =>
                {
                    _stack.Clear(); // wipe out anything that someone might have pushed on to the stack before this line.
                    for (int i = 1; i <= MAX_SIZE; i++)
                    {
                        _stack.Push(i);
                    }
                });
            "When Pushing an item on to the stack"
                .x(() => ex = Assert.Throws<FullStackException>(() => _stack.Push(300)));
            "Then FullStackException is thrown"
                .x(() => Assert.Equal(Constants.StackIsFull, ex.Message));
        }

        [Fact]
        public void Clearing_a_stack_with_items()
        {
            "Given a stack with items"
                .x(() =>
                {
                    _stack.Clear(); // wipe out anything that someone might have pushed on to the stack before this line.
                    for (int i = 1; i <= 10; i++)
                    {
                        _stack.Push(i);
                    }
                });
            "When Clearing the stack"
                .x(() => _stack.Clear());
            "Then Count should now be 0"
                .x(() => Assert.Equal(0, _stack.Count));
        }


        [Fact]
        public void Clearing_an_empty_stack()
        {
            "Given an empty stack"
                .x(() =>
                {
                    _stack.Clear(); // wipe out anything that someone might have pushed on to the stack before this line.
                });
            "When Clearing the stack"
                .x(() => _stack.Clear());
            "Then Count should still be 0"
                .x(() => Assert.Equal(0, _stack.Count));
        }

        [Fact]
        public void Contains_should_return_true_for_existing_item()
        {
            var values = new int[] { 1, 3, 5, 6, 9 };
            bool containsResult = false;

            "Given a stack with items"
                .x(() =>
                {
                    _stack.Clear(); // wipe out anything that someone might have pushed on to the stack before this line.
                    for (int i = 0; i < values.Length; i++)
                    {
                        _stack.Push(values[i]);
                    }
                });
            "When calling Contains for existing item"
                .x(() =>
                {
                    // any of the values push onto the stack should return true
                    int randIndex = new Random().Next(0, 4);
                    containsResult = _stack.Contains(values[randIndex]);
                });
            "Then [true] should be returned"
                .x(() => Assert.True(containsResult));
        }

        [Fact]
        public void Contains_should_return_false_for_non_existing_item()
        {
            var values = new int[] { 1, 3, 5, 6, 9 };
            bool containsResult = false;

            "Given a stack with items"
                .x(() =>
                {
                    _stack.Clear(); // wipe out anything that someone might have pushed on to the stack before this line.
                    for (int i = 0; i < values.Length; i++)
                    {
                        _stack.Push(values[i]);
                    }
                });
            "When calling Contains for non-existing item"
                .x(() =>
                {
                    //note: 100 is not in the stack.
                    containsResult = _stack.Contains(100);
                });
            "Then [false] should be returned"
                .x(() => Assert.False(containsResult));
        }

        
        [Fact]
        public void Contains_should_return_false_when_stack_is_empty()
        {
            bool containsResult = false;

            "Given an empty stack"
                .x(() =>
                {
                    _stack.Clear(); // wipe out anything that someone might have pushed on to the stack before this line.
                });
            "When calling Contains"
                .x(() =>
                {
                    //note: 100 is not in the stack.
                    containsResult = _stack.Contains(100);
                });
            "Then [false] should be returned"
                .x(() => Assert.False(containsResult));
        }
    }
}
