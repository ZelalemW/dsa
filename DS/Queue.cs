using System;
using System.Collections;
using System.Collections.Generic;
using DS.Exceptions;

namespace DS
{
    /// <summary>
    /// Queue implemntation using Doubly Linked List as a back store.
    /// First In First Out (FIFO)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Queue<T> : IEnumerable<T>
    {
        /// <summary>
        /// Storage - using Doubly Linked List
        /// </summary>
        private DS.LinkedList<T> _store;

        public Queue()
        {
            _store = new DS.LinkedList<T>();
        }

        /// <summary>
        /// Returns true if the queue is empty. Otherwise, false will be returned.
        /// </summary>
        public bool IsEmpty => _store == null || _store.IsEmpty;

        /// <summary>
        /// Returns the number of items in the queue.
        /// </summary>
        public int Count => _store == null ? 0 : _store.Count;

        /// <summary>
        /// Item in the front of the queue. This won't dequeue the item from the queue.
        /// </summary>
        public T First
        {
            get
            {
                if (IsEmpty)
                {
                    throw new EmptyQueueException();
                }

                return _store.Head;
            }
        }

        /// <summary>
        /// Item in the rear of the queue. This won't dequeue the item from the queue.
        /// </summary>
        public T Last
        {
            get
            {
                if (IsEmpty)
                {
                    throw new EmptyQueueException();
                }

                return _store.Tail;
            }
        }

        /// <summary>
        /// Adds the item to the tail of the queue
        /// </summary>
        /// <param name="item">item to be added to the tail of the queue</param>
        public void Enqueue(T item)
        {
            _store.AddToTail(item);
        }

        /// <summary>
        /// Removes the oldest item from the queue.
        /// </summary>
        /// <returns>The oldest item in the queue</returns>
        public T Dequeue()
        {
            if (IsEmpty)
            {
                throw new EmptyQueueException();
            }

            T headItem = _store.RemoveHead();

            return headItem;
        }

        public bool Contains(T item)
        {
            if (IsEmpty)
            {
                return false;
            }

            return _store.Contains(item);
        }

        /// <summary>
        /// Clears the queue
        /// </summary>
        public void Clear()
        {
            if (!IsEmpty)
            {
                _store.Clear();
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (IsEmpty)
            {
                throw new EmptyQueueException();
            }

            return _store.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}