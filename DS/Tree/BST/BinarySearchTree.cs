using System;
using System.Collections.Generic;
using System.Collections;

namespace DS
{
    /// <summary>
    /// Class representing Binary Search Tree data structure
    /// </summary>
    public class BinarySearchTree<T> where T : IComparable<T>
    {
        /// <summary>
        /// Reference to the root node.
        /// </summary>
        private DS.Node<T> _root;

        //The number of items in a tree.
        private int _count;

        /// <summary>
        /// Returns the number of items in BST
        /// </summary>
        public int Count => _count;

        /// <summary>
        /// Returns true if BST is empty. Otherwise, false will be returned.
        /// </summary>
        public bool IsEmpty => _root == null;

        public BinarySearchTree()
        {
            _root = null;
            _count = 0;
        }

        /// <summary>
        /// Inserts an item into BST.
        /// </summary>
        /// <param name="item">Item to insert</param>
        public void Insert(T item)
        {
            var newNode = new DS.Node<T>(item);

            if (IsEmpty)
            {
                _root = newNode;
                _count++;
            }
            else
            {
                var parent = _root;
                var child = _root;

                while (true)
                {
                    if (item.CompareTo(parent.Item) < 0)
                    {
                        if (parent.Left == null)
                        {
                            parent.Left = newNode;
                            _count++;
                            break;
                        }
                        else
                        {
                            parent = parent.Left;
                        }
                    }
                    else
                    {
                        if (parent.Right == null)
                        {
                            parent.Right = newNode;
                            _count++;
                            break;
                        }
                        else
                        {
                            parent = parent.Right;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Searchs an element in the tree.
        /// </summary>
        /// <param name="item">item to search for</param>
        /// <returns>the element found in the BST or default value, if not found</returns>
        public T Search(T item)
        {
            if (IsEmpty)
            {
                return default(T);
            }

            if (_root.Item.CompareTo(item) == 0)
            {
                return _root.Item;
            }

            var current = item.CompareTo(_root.Item) < 0 ? _root.Left : _root.Right;

            return Search(current, item);
        }

        /// <summary>
        /// Deletes a node from BST
        /// </summary>
        /// <param name="item">item to delete</param>
        public void Delete(T item)
        {
            /*
                Cases:
                1. If BST is empty => throw exception
                2. If item is the only item in the BST => Reset the BST
                3. If item is leaf in the BST => Let the parent no longer point to it.
                4. If item has one child => Let the item's parent connect to the child & let go of it.
                5. If the item has two children: => 
                       option 1: a) find the largest of the left sub-tree, lets call it Z
                                 b) replace the item with Z
                                 c) nuke down Z
                       option 2: a) find the smallest of the right sub-tree, lets call it Z
                                 b) replace the item with Z
                                 c) nuke down Z
             */

            if (IsEmpty)
            {
                throw new EmptyBSTException();
            }

            var hasOnlyOneNode = _root.Left == null &&
                                    _root.Right == null;

            if (hasOnlyOneNode)
            {
                _root = null;
                _count = 0;
                return;
            }

            var parent = _root;
            var target = _root;

            while (target != null && item.CompareTo(target.Item) != 0)
            {
                parent = target;
                target = item.CompareTo(target.Item) < 0
                ? target.Left
                : target.Right;
            }

            if (target == null)
            {
                throw new ItemNotFoundException(Constants.ItemNotFoundInBST);
            }

            if (target.IsLeaf)
            {
                // if its right child, nullify parent's RIGHT to forget the right child.
                if (parent.Right == target)
                {
                    parent.Right = null;
                }
                else // if its the left child, nullify parent' LEFT to forget the left child
                {
                    parent.Left = null;
                }
            }
            else if (target.Left != null && target.Right != null)
            {
                // option 1: find largest of left sub-tree
                Node<T> parentOfLargestLeafNode;
                var largestLeafNodeOfLeftSubTree = GetLargestLeafNode(target.Left, out parentOfLargestLeafNode);

                // replace the target node's value to be the [largest leaf node's] value --> this is equivalent to DELETING it
                target.Item = largestLeafNodeOfLeftSubTree.Item;

                // now, nuke out the largest leaf node - since its value has been copied over to the target node anyway.
                bool isLargestLeafRightChild = parentOfLargestLeafNode.Right == largestLeafNodeOfLeftSubTree;

                if (isLargestLeafRightChild)
                {
                    parentOfLargestLeafNode.Right = null;
                }
                else
                {
                    parentOfLargestLeafNode.Left = null;
                }

                //// option 2: 
                /// --> find smallest of right sub-tree
                //var smallestLeafNodeOfRightSubTree = GetSmallestNode(target.Right, out parentOfLargestLeafNode);
                //// replace the target node's value to be the [smallest leaf node's] value --> this is equivalent to DELETING it
                //target.Item = smallestLeafNodeOfRightSubTree.Item;

                //// now, nuke out the largest leaf node - since its value has been copied over to the target node anyway.
                //bool isSmallestLeafRightChild = parentOfLargestLeafNode.Right == smallestLeafNodeOfRightSubTree;

                //if (isLargestLeafRightChild)
                //{
                //    parentOfLargestLeafNode.Right = null;
                //}
                //else
                //{
                //    parentOfLargestLeafNode.Left = null;
                //}
            }
            else // have one child.
            {
                // connect parent to point to grand-child and let go of the child
                var isTargetRightChild = parent.Right == target;
                var targetHasLeftChild = target.Right == null;
                var childOfTarget = targetHasLeftChild
                                    ? target.Left
                                    : target.Right;

                if (isTargetRightChild)
                {
                    parent.Right = childOfTarget;
                }
                else
                {
                    parent.Left = childOfTarget;
                }
            }

            _count--;
        }

        /// <summary>
        /// Gets the items in PreOrder traversal sequence
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> PreOrder()
        {

            if (IsEmpty)
            {
                throw new EmptyBSTException();
            }

            var current = _root;

            var preOrderTraversal = new List<T>();
            PreOrder(current, preOrderTraversal);

            return preOrderTraversal;
        }

        /// <summary>
        /// Gets BST items in InOrder traversal order
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> InOrder()
        {
            if (IsEmpty)
            {
                throw new EmptyBSTException();
            }

            var inOrderTraversal = new List<T>();
            var current = _root;

            InOrder(current, inOrderTraversal);

            return inOrderTraversal;
        }

        /// <summary>
        /// Gets the BST items in PostOrder traversal order
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> PostOrder()
        {
            var postOrderTraversal = new List<T>();

            if (IsEmpty)
            {
                throw new EmptyBSTException();
            }

            var current = _root;

            PostOrder(current, postOrderTraversal);

            return postOrderTraversal;
        }

        /// <summary>
        /// Gets the height of a tree.
        /// </summary>
        /// <returns>The height of a tree</returns>
        public int GetHeight()
        {
            if (IsEmpty)
            {
                return 0;
            }

            var current = _root;

            return GetHeight(current);
        }

        #region - helper methods

        /// <summary>
        /// Gets the largest leaf node from the given sub-tree
        /// </summary>
        /// <param name="current">the root for the sub-tree</param>
        /// <returns>larest leaf node with in the sub-tree</returns>
        private Node<T> GetLargestLeafNode(Node<T> current, out Node<T> parent)
        {
            parent = current ?? throw new ArgumentNullException($"{nameof(current)} cannot be null.");

            //until you get to the the leaf node - keep walking
            while (!current.IsLeaf)
            {
                parent = current;
                current = current.Right != null
                            ? current.Right
                            : current.Left;
            }

            return current;
        }

        /// <summary>
        /// Gets the smallest leaf node in the given sub-tree
        /// </summary>
        /// <param name="current">the root node for the sub-tree </param>
        /// <param name="parent">the parent of the smallest leaf node in the sub-tree </param>
        /// <returns>Smallest leaf node of the sub-tree</returns>
        private Node<T> GetSmallestNode(Node<T> current, out Node<T> parent)
        {
            parent = current ?? throw new ArgumentNullException($"{nameof(current)} cannot be null.");

            //until you get to the smallest leaf node -- keep walking
            while (!current.IsLeaf)
            {
                parent = current;
                current = current.Left != null
                        ? current.Left
                        : current.Right;
            }

            return current;
        }


        private T Search(Node<T> current, T item)
        {
            if (current == null)
            {
                return default(T);
            }

            if (current.Item.CompareTo(item) == 0)
            {
                return current.Item;
            }

            current = item.CompareTo(current.Item) < 0 ? current.Left : current.Right;

            return Search(current, item);
        }

        private void PreOrder(Node<T> current, IList<T> preOrderTraversal)
        {
            if (current != null)
            {
                preOrderTraversal.Add(current.Item);
                PreOrder(current.Left, preOrderTraversal);
                PreOrder(current.Right, preOrderTraversal);
            }
        }

        private void InOrder(Node<T> current, IList<T> inOrderTraversal)
        {
            if (current != null)
            {
                InOrder(current.Left, inOrderTraversal);
                inOrderTraversal.Add(current.Item);
                InOrder(current.Right, inOrderTraversal);
            }
        }

        private void PostOrder(Node<T> current, IList<T> postOrderTraversal)
        {
            if (current != null)
            {
                PostOrder(current.Left, postOrderTraversal);
                PostOrder(current.Right, postOrderTraversal);
                postOrderTraversal.Add(current.Item);
            }
        }

        private int GetHeight(Node<T> current)
        {
            if (current == null)
            {
                return 0;
            }

            return Math.Max(GetHeight(current.Left), GetHeight(current.Right)) + 1;
        }

        #endregion

        // public IEnumerator<T> GetEnumerator()
        // {
        //     if (IsEmpty)
        //     {
        //         throw new EmptyBSTException();
        //     }

        //     DS.Queue<Node<T>> queue = new Queue<Node<T>>();
        //     queue.Enqueue(_root);
        //     Node<T> current = null;

        //     do
        //     {
        //         current = queue.Dequeue();

        //         yield return current.Item;

        //         if (current.Left != null)
        //         {
        //             queue.Enqueue(current.Left);
        //         }

        //         if (current.Right != null)
        //         {
        //             queue.Enqueue(current.Right);
        //         }
        //     } while (!queue.IsEmpty);
        // }

        // IEnumerator IEnumerable.GetEnumerator()
        // {
        //     return GetEnumerator();
        // }
    }
}