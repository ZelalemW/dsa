using System;
using System.Collections.Generic;
using System.Linq;
using Common.Extensions;

namespace DS
{
    /// <summary>
    /// Represents Suffix Trie data structure
    /// </summary>
    public class Trie
    {
        /// <summary>
        /// Reference to the root node
        /// </summary>
        private static TrieNode _root;

        /// <summary>
        /// Returns true if the Trie data structure is empty. Otherwise, false will be returned.
        /// </summary>
        /// <returns>True if the Trie data structure is empty. Otherwise, false will be returned.</returns>
        public bool IsEmpty => _root.IsNull() || 
                    _root.Children.IsNull() || 
                    !_root.Children.Any();
        
        /// <summary>
        /// Creates a new instance of Trie
        /// </summary>
        public Trie()
        {
            _root = new TrieNode();
        }

        /// <summary>
        /// Creates a suffix Trie data structure for a given text
        /// </summary>
        /// <param name="str">text where you would like to build a suffix Trie for.</param>
        public void CreateSuffixTrieFor(string str)
        {
            if(str.IsNull())
            {
                throw new ArgumentNullException($"{nameof(str)} cannot be null.");
            }

            var suffices = str.GetAllSuffices();

            for (int suffixStartIndex = 0; suffixStartIndex < suffices.Count(); suffixStartIndex++)
            {
                Insert(suffices.ElementAt(suffixStartIndex), suffixStartIndex);
            }
        }

        /// <summary>
        /// Inserts a sequence of character of the given word - i.e, sequence of characters into a Trie
        /// </summary>
        /// <param name="word">text or sequence of characters to be added into the Trie data structure.</param>
        /// <param name="index">start index of the word in the main text.</param>
        private void Insert(string word, int index)
        {
            //make sure the root reference isn't null.
            if (_root.IsNull())
            {
                _root = new TrieNode();
            }

            TrieNode current = _root;

            for (int i = 0; i < word.Length; i++)
            {
                current.Children = current.HasChildren 
                                ? current.Children 
                                : new Dictionary<char, TrieNode>();

                if (current.Children.ContainsKey(word[i]))
                {
                    current = current.Children[word[i]];
                }
                else
                {
                    var isLastIndex = i == word.Length - 1;
                    var startIndex = isLastIndex ? index : (int?)null;
                    var newNode = new TrieNode(character:word[i], startIndex:startIndex, isWordEnding: isLastIndex);
                    current.Children[word[i]] = newNode;
                    current = newNode;
                }
            }

            // set word ending here for word that every character was found in existing Trie.
            current.IsWordEnding = true;
            current.StartIndex = index;
        }

        /*
        TODO: Implement the following
        1. Find out if a string s is substring of T
        2. Find out if a string s is suffix of T 
        3. Find out the longest repeated substring text of T
        4. Find out indexes of substring in T
        5. Append $ to the Trie
        */

        public bool IsSubstring(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                throw new ArgumentNullException($"{nameof(word)} cannot be null or empty.");
            }

            if (IsEmpty)
            {
                return false;
            }

            TrieNode current = _root;

            for (int i = 0; i < word.Length; i++)
            {
                if (!current.Children.ContainsKey(word[i]))
                {
                    return false;
                }
                
                current = current.Children[word[i]];
            }

            return true ;
        }

        public int SubstringCount(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                throw new ArgumentNullException($"{nameof(word)} cannot be null or empty.");
            }

            if (IsEmpty)
            {
                return 0;
            }

            int count = 0;

            TrieNode current = _root;

            for (int i = 0; i < word.Length; i++)
            {
                if (!current.Children.ContainsKey(word[i]))
                {
                    return 0;
                }
                
                current = current.Children[word[i]];
            }

            var stack = new Stack<TrieNode>();
            stack.Push(current);

            while(stack.Count >0)
            {
                var topNode = stack.Pop();

                foreach (var childNode in topNode.Children)
                {
                    stack.Push(childNode.Value);
                }

                if(topNode.IsWordEnding)
                {
                    count++;
                }
            }

            return count;
        }
        
        /// <summary>
        /// Gets all words in a Trie starting with the prefix
        /// </summary>
        /// <param name="prefix">Prefix (one or more sequence of characters) used as a prefix for all words we are looking for.</param>
        /// <returns>All words (texts) where the provided prefix text is a prefix of.</returns>
        public IEnumerable<string> AllWordsWithPrefix(string prefix)
        {
            if(string.IsNullOrEmpty(prefix))
            {
                throw new ArgumentNullException($"{nameof(prefix)} cannot be null or empty.");
            }

            TrieNode node = _root;
            var allWords = new List<string>();

            for (int i = 0; i < prefix.Length; i++)
            {
                node = node.Children[prefix[i]];
            }

            Collect(node, prefix, allWords);

            return allWords;
        }

        /// <summary>
        /// Collapses the tree - when there is no branching - this is to optimize space utilization.
        /// </summary>
        private void Collapse()
        {
            // for every leaf node
            // until you reach to a parent with more than one child, keep collapsing nodes
        }

        private void Collect(TrieNode node, string prefix, IList<string> allWords)
        {
            if (node.IsNull())
            {
                return;
            }

            if (node.IsLeaf)
            {
                allWords.Add(prefix);
            }

            foreach (var childNode in node.Children)
            {
                if (childNode.Value.IsNull())
                {
                    continue;
                }

                Collect(childNode.Value, prefix + childNode.Value.Character, allWords);
            }
        }
    }
}
