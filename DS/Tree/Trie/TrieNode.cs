using System.Collections.Generic;
using System.Linq;

namespace DS
{
    /// <summary>
    /// A Node representing one node in a Trie tree structure
    /// </summary>
    public class TrieNode
    {
        /// <summary>
        /// Character stored in a node. 
        /// </summary>
        /// <value></value>
        public char Character { get; set; }

        /// <summary>
        /// Zero or more children nodes.
        /// </summary>
        /// <value></value>
        public IDictionary<char, TrieNode> Children { get; set; }

         /// <summary>
        /// Returns true if its a word ending. Otherwise, false will be returned.
        /// </summary>
        /// <value>True will be returned if the node is a word ending. Otherwise, false will be returned.</value>
        public bool IsWordEnding { get; set; }
        
        /// <summary>
        /// Start index - if the node is a word ending. null will be used for any node that is not a word ending.
        /// </summary>
        /// <value></value>
        public int? StartIndex { get; set; }

        /// <summary>
        /// Checks if the node is a leaf node - with no child
        /// </summary>
        /// <returns>True is returned if the node is a leaf node. Otherwise, false will be returned</returns>
        public bool IsLeaf => !Children.Any(); // [FIXME] - WHAT IF Children is NULL. HMMMM, do something here...

        /// <summary>
        /// Checks if the node have one or more children
        /// </summary>
        /// <returns>True if the node has children. Otherwise, false will be returned.</returns>
        public bool HasChildren => Children != null && Children.Any();

        /// <summary>
        /// Instantiates a new instance of Node.
        /// </summary>
        public TrieNode()
        {
            Children = new Dictionary<char, TrieNode>();
            StartIndex = null;
            IsWordEnding = false;
        }

        public TrieNode(char character):this()
        {
            Character=character;
        }

        public TrieNode(char character, int? startIndex, bool isWordEnding) : this(character)
        {
            StartIndex = startIndex;
            IsWordEnding = isWordEnding;
        }
    }
}