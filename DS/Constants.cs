﻿namespace DS
{
    public class Constants
    {
        public const string StackIsEmpty = "Stack is empty.";
        public const string StackIsFull = "Stack is full.";
        public const string LinkedListIsEmpty = "Linked list is empty.";
        public const string QueueIsEmpty = "Queue is empty.";
        public const string BinarySearchTreeIsEmpty = "Binary Search Tree (BST) is empty.";

        public const string ItemNotFoundInBST = "Item not found in BST.";
    }
}
