﻿using System.Collections;
using System.Collections.Generic;
using DS.Exceptions;

namespace DS
{
    /// <summary>
    /// Stack data structure - Last In First Out (LIFO) implementation using Array as a back store.
    /// </summary>
    /// <typeparam name="T">The type to be stored.</typeparam>
    public class Stack<T> : IEnumerable<T>
    {
        /// <summary>
        /// Index of the last item push onto the stack
        /// </summary>
        private int _topIndex;

        /// <summary>
        /// Size of the storage - initialized to 100 for now. But this can either be set to a different value OR stack size can also be 
        /// dynamically resized until we run out of memory exception. 
        /// Setting it to a large value may be wasteful if most of it won't be used. Resize as needed (such as by doubling array size when full).
        /// </summary>
        private uint SIZE = 100;

        /// <summary>
        /// Maximum stored items count. Any attempt beyond this will throw StackIsFullException
        /// </summary>
        private uint MAX_SIZE = 1000;

        /// <summary>
        /// Storage used for our stacked items
        /// </summary>
        private T[] _store;

        /// <summary>
        /// Returns number of items on the stack
        /// </summary>
        public int Count => _topIndex + 1;

        /// <summary>
        /// Retruns if stack is empty
        /// </summary>
        public bool IsEmpty => _topIndex == -1;

        /// <summary>
        /// Constructors - initializes a new instanc of a stack.
        /// </summary>
        public Stack()
        {
            Init();
        }

        /// <summary>
        /// API to push an item onto the top of the stack.
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="StackIsFullException">
        /// throws if stack is full.
        /// </exception>
        public void Push(T item)
        {
            var isCurrentStorageFull = _topIndex == _store.Length - 1;

            if (isCurrentStorageFull)
            {
                var canResize = _store.Length < MAX_SIZE;

                if (!canResize)
                {
                    throw new FullStackException();
                }

                ResizeStorage();
            }

            _topIndex++;
            _store[_topIndex] = item;
        }


        /// <summary>
        /// Pops the top element and return it
        /// </summary>
        /// <returns>Top item</returns>
        public T Pop()
        {
            if (IsEmpty)
            {
                throw new EmptyStackException();
            }

            T topItem = _store[_topIndex];
            _topIndex--; // this is to mark the item as deleted

            return topItem;
        }

        /// <summary>
        /// Gets the top item. Unlike Pop(), this API won't delete the top item.
        /// </summary>
        /// <returns>Top item on the stack</returns>
        public T Peek()
        {
            if (IsEmpty)
            {
                throw new EmptyStackException();
            }

            T topItem = _store[_topIndex];

            return topItem;
        }

        public void Clear()
        {
            Init();
        }

        ///// <summary>
        ///// Gets the top item. Unlike Pop(), this API won't delete the top item.
        ///// </summary>
        ///// <returns>Top item on the stack</returns>
        //public T Top()
        //{
        //    if (IsEmpty)
        //    {
        //        throw new EmptyStackException();
        //    }

        //    T topItem = _store[_topIndex];

        //    return topItem;
        //}

        /// <summary>
        /// Resizes storage by doubling size until it reachs MAX_SIZE
        /// </summary>
        private void ResizeStorage()
        {
            var newStorageSize = _store.Length * 2 <= MAX_SIZE
                                ? _store.Length * 2
                                : (int)MAX_SIZE;

            T[] tempStorage = new T[newStorageSize];

            for (int i = 0; i < _store.Length; i++)
            {
                tempStorage[i] = _store[i];
            }

            _store = tempStorage;
        }

        private void Init()
        {
            _topIndex = -1;
            _store = new T[SIZE];
        }

        public bool Contains(T item)
        {
            if (IsEmpty)
            {
                return false;
            }
            
            foreach (var stackItem in _store)
            {
                if (stackItem != null && stackItem.Equals(item))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets enumerator for the stack collection
        /// </summary>
        /// <returns></returns>
        public IEnumerator<T> GetEnumerator()
        {
            if (IsEmpty)
            {
                throw new EmptyStackException();
            }

            for (int i = _topIndex; i >= 0; i--)
            {
                yield return _store[i];
            }
        }

        /// <summary>
        /// Gets enumerator for the stack collection
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
