using System;

namespace DS
{
    internal class Node<T>
    {
        internal T Item { get; set; }
        internal Node<T> Left { get; set; }
        internal Node<T> Right { get; set; }

        /// <summary>
        /// Instantiates a new isolated instance of Node.
        /// </summary>
        /// <param name="item"></param>
        internal Node(T item)
        {
            Item = item;
            Left = null;
            Right = null;
        }

        internal bool IsLeaf => Left == null && Right == null;
    }
}