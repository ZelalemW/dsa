using System;

namespace DS.Exceptions
{
    public class EmptyLinkedListException : Exception
    {
        public EmptyLinkedListException() : base(Constants.LinkedListIsEmpty)
        {

        }
    }
}
