using System;

namespace DS
{
    public class EmptyBSTException : Exception
    {
        public EmptyBSTException() : base(Constants.BinarySearchTreeIsEmpty)
        {
        }
    }
}