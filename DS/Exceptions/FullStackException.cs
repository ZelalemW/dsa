﻿using System;

namespace DS.Exceptions
{
    public class FullStackException : Exception
    {
        public FullStackException() : base(Constants.StackIsFull)
        {

        }
    }
}
