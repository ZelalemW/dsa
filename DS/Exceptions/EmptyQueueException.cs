using System;

namespace DS.Exceptions
{
    public class EmptyQueueException : Exception
    {
        public EmptyQueueException() : base(Constants.QueueIsEmpty)
        {

        }
    }
}
