﻿using System;

namespace DS.Exceptions
{
    public class EmptyStackException : Exception
    {
        public EmptyStackException() : base(Constants.StackIsEmpty)
        {

        }
    }
}
