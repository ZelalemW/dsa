using System;
using System.Collections;
using System.Collections.Generic;
using DS.Exceptions;

namespace DS
{
    /// <summary>
    /// Doubly linked list
    /// </summary>
    /// <typeparam name="T">Type of data to be stored</typeparam>
    public class LinkedList<T> : IEnumerable<T>
    {
        /// <summary>
        /// Reference to the head of the linked list
        /// </summary>
        private Node<T> _head;

        /// <summary>
        /// Reference to the tail of the linked list.
        /// </summary>
        private Node<T> _tail;

        private int _count = 0;

        //Returns true if the list is empty. Otherwise, false will be returned.
        public bool IsEmpty => _head == null;

        /// <summary>
        /// Returns the number of items in the list
        /// </summary>
        public int Count => _count;

        /// <summary>
        /// The first item in the list
        /// </summary>
        public T Head
        {
            get
            {
                if (IsEmpty)
                {
                    throw new EmptyLinkedListException();
                }

                return _head.Item;
            }
        }
        
        /// <summary>
        /// The last item in the list
        /// </summary>
        public T Tail
        {
            get
            {
                if (IsEmpty)
                {
                    throw new EmptyLinkedListException();
                }

                return _tail.Item;
            }
        }

        /// <summary>
        /// Adds the given item to the head of the linked list.
        /// </summary>
        /// <param name="item">Item to be added to the list</param>
        public void AddToHead(T item)
        {
            Node<T> newNode = new Node<T>(item); // note - newNode is an isolated instance at this point.

            if (IsEmpty) //if there is nothing in the list yet, make it to be both the head and tail of the list.
            {
                _head = newNode;
                _tail = _head;
            }
            else
            {
                //1. let newNode Right point the existing Head item.
                newNode.Right = _head;
                //2. let existing Head.Left now point, newNode
                _head.Left = newNode;
                //3. update _head to be the newly added newNode.
                _head = newNode;

                //NOTE: step 1 and 2 can appear in any order. 
            }

            _count++;
        }

        /// <summary>
        /// Adds the given item to the tail of the linked list.
        /// </summary>
        /// <param name="item">Item to be added to the list</param>
        public void AddToTail(T item)
        {
            Node<T> newNode = new Node<T>(item);

            if (IsEmpty) //if there is nothing in the list yet, make the new one to be both head and tail.
            {
                _head = newNode;
                _head = _tail;
            }
            else
            {
                //1. let _tail Right point the new node.
                _tail.Right = newNode;

                //2. let new node's Left point the existing _tail
                newNode.Left = _tail;

                //3. update _tail to be the newly added newNode.
                _tail = newNode;

                //NOTE: step 1 and 2 can appear in any order.
            }

            _count++;
        }

        /// <summary>
        /// Removes the head item and return it.
        /// </summary>
        /// <returns>Item at the head of the linked list</returns>
        public T RemoveHead()
        {
            if (IsEmpty)
            {
                throw new EmptyLinkedListException();
            }

            T headItem = _head.Item;

            // if linked has only one item
            if (_head.Right == null)
            {
                _head = null;
                _tail = null;
            }
            else
            {
                _head = _head.Right;
                _head.Left.Right = null;  // --> x
                _head.Left = null;  // x <--
            }

            _count--;

            return headItem;
        }

        /// <summary>
        /// Removes the tail item in the list and return it.
        /// </summary>
        /// <returns>The item at the tail of the linked list</returns>
        public T RemoveTail()
        {
            if (IsEmpty)
            {
                throw new EmptyLinkedListException();
            }

            T tailItem = _tail.Item;

            //if there is only one item in the list
            if (_tail.Left == null)
            {
                _head = null;
                _tail = null;
            }
            else
            {
                _tail = _tail.Left;
                _tail.Right.Left = null; // x <--
                _tail.Right = null;  // --> x
            }

            _count--;

            return tailItem;
        }

        /// <summary>
        /// Clears the list.
        /// </summary>
        public void Clear()
        {
            _head = null;
            _tail = null;
        }

        /// <summary>
        /// Checks if the item is in the list.
        /// </summary>
        /// <param name="item">item to check</param>
        /// <returns>true if the item is in the list. Otherwise, false will be returned.</returns>
        public bool Contains(T item)
        {
            var exists = false;

            if (IsEmpty)
            {
                return exists;
            }

            var current = _head;

            while (current != null)
            {
                if (current.Item != null && current.Item.Equals(item))
                {
                    exists = true;

                    break;
                }
            }

            return exists;
        }

        /// <summary>
        /// Gets iterator - this helps to use foreach on the list.
        /// </summary>
        /// <returns>enumerator of the list</returns>
        public IEnumerator<T> GetEnumerator()
        {
            var current = _head;

            while (current != null)
            {
                yield return current.Item;

                current = current.Right;
            }
        }

        /// <summary>
        /// Gets enumerator so that foreach can be used on the list.
        /// </summary>
        /// <returns>enumerator of the list</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}