# Data Structure & Algorithms

Before jumping into the implementation, please read the document corresponding to the data structure or algorithm you are interested.

Implementation for data structures can be found under project *[DS]*

Implementation for *Algorithms* can be found under project *[Algorithm]*

Tests using BDD style can be found under *[Tests]* directory

**Data Structures** covered includes:

- Linked List
- Stack
- Queue

---

## Stack

  *Last In First Out (*LIFO*)*

![Stack](https://www.geeksforgeeks.org/wp-content/uploads/gq/2013/03/stack.png)

[Source: geeksforgeeks](https://www.geeksforgeeks.org/wp-content/uploads/gq/2013/03/stack.png)

***APIs***:

- ***Push(item)***: Puts an item to the top of the stack. If the stack is full to its maximum capacity, then exception will be thrown.

- ***Pop()***: Remove the top item from the stack and return it. If stack is empty, an exception will be thrown.

- ***Top()*** or ***Peek()***: Returns top element on the stack without deleting it. This operation has no side effect. If the stack is empty, an exception will be thrown.

- ***IsEmpty***: Returns true if stack has no item in it.

- ***Count***: Returns the number of items on the stack.

- ***Clear()***: Clears the stack - basically removing all items (if any) and leave it as a clean slate.

**Applications**:

- Syntax parsing
- Reversing order (eg. string)
- Undo mechanism in a text editor
- Backtracking
- Expression conversion (prefix, infix, postfix)
- Compiler syntax check for maching braces
- Function call execution. For example:
  - If M1() calls M2(), and
  - M2() calls M3() and
  - M3() calls M4()
  - then M4() need to complete first before M3 finishes. And M3 has to finish before M2() and so on until we reach the top level caller method.
  - This of course assumes that method calls are synchronous calls and not a fire and forget async method calls.

## Tests

[Test cases using (Given, When, Then) style is found here.](https://bitbucket.org/ZelalemW/dsa/src/master/Tests/DS.Tests/test-cases/Queue-test-cases.md)

[Test source code](https://bitbucket.org/ZelalemW/dsa/src/master/Tests/DS.Tests/StackFeature.cs)

---

## Queue

First In First Out (*FIFO*)

![Queue](https://www.geeksforgeeks.org/wp-content/uploads/gq/2014/02/Queue.png)

[Source: geeksforgeeks](https://www.geeksforgeeks.org/wp-content/uploads/gq/2014/02/Queue.png)

***APIs***:

- *Enqueue*: Adds an item to the tail of the queue.

- *Dequeue*: Remove the oldest item from the queue and return its data. If queue is empty, an exception will be thrown.

- *Contains*: Checks to see if an item is in the queue or not. It returns true if it does exist. Otherwise, false will be returned.

- *IsEmpty*: Returns true if the queue has no item in it.

- *Count*: Returns the number of items in the queue.

- *Clear*: Clears the queue.

**Applications**:

- Printing Jobs
- CPU scheduling by the Operating System
- Web Servers acepting and processing user requeusts.
- Threadpool threads with multi-core machine: having one queue for each core but with the possibility of idle thread to steal work from busy core's queue.

- Event Loop in JavaScript / NodeJs

- Handling of Interrupts in real time communication
- Inter-Process communication usually is designed using queue.
- and much more.

**Tests**:

[Test cases using (Given, When, Then) style is found here.](https://bitbucket.org/ZelalemW/dsa/src/master/Tests/DS.Tests/test-cases/Queue-test-cases.md)

[Test source code](https://bitbucket.org/ZelalemW/dsa/src/master/Tests/DS.Tests/QueueFeature.cs)

## Tree

Hierarchical data structure. Comes in many flavors. We will explore few variants - Binary Search Tree (**BST**) being the main focus.

![Tree](https://www.cdn.geeksforgeeks.org/wp-content/uploads/binary-tree-to-DLL.png)
[Source](https://www.cdn.geeksforgeeks.org/wp-content/uploads/binary-tree-to-DLL.png)

***Node***:

A node is a structure which may contain a value or condition, or represent a separate data structure.

***Root***:

The top node in a tree, the prime ancestor.

***Child***:

A node directly connected to another node when moving away from the root, an immediate descendant.

***Siblings***:
A group of nodes with the same parent.

***Descendant***:

A node reachable by repeated proceeding from parent to child. Also known as subchild.

***Ancestor***:

A node reachable by repeated proceeding from child to parent.

***Leaf***:

A node with no children.

***Path***:

A sequence of nodes and edges connecting a node with a descendant.

***Depth***:

The distance between a node and the root.

***Height***:

The number of edges on the longest path between a node and a descendant leaf.

***Sub Tree***:

A tree T is a tree consisting of a node in T and all of its descendants in T.

***Size***:

Number of nodes in the tree.

## Suffix Trie

This is another *Tree* structure used for high performance string search/match algorithms.

Design and implemented right, it can significantly improve performance of looking up a given pattern in a very long text such as *genome* sequence.

Using brute force algorithms for such task would be highlighly time taking and impractical for use.

That said, constructing a *Trie* data structure and holding it in memory consumes both space and time. The look up afterwards will be where it shines!

![Suffix Trie](https://i.postimg.cc/qqtvwzKj/TrieDS.png)
Figure: 4. Suffix Trie Data Structure for text: "abaaba"

### Modeling

Here is how to model a Trie Data Structure.

*Step 1* :

**Start from the simplest - Reprepresent a Single *Node***

If you look carefully, Figure 4 is composed of *Nodes*. Now, before we think about the whole *Tree* structure, we need to look a node and figure out how to represent.
Here is how you can approach it. A node as you can see is an object that have:

- A character such as *a*, *b* as shown in figure 4.
- References to other (*children*) nodes.
- A flag indicating if the node is a suffix ending or not.
- An index with value indicating the start index of the suffix navigated from the root node down to the current node - if the node is a word (suffix) ending.

In code, this would look like the following:

```cs

using System.Collections.Generic;
using System.Linq;

namespace DS
{
    /// <summary>
    /// A Node representing one node in a Trie tree structure
    /// </summary>
    public class TrieNode
    {
        /// <summary>
        /// Character stored in a node.
        /// </summary>
        /// <value></value>
        public char Character { get; set; }

        /// <summary>
        /// Zero or more children nodes.
        /// </summary>
        /// <value></value>
        public IDictionary<char, TrieNode> Children { get; set; }

         /// <summary>
        /// Returns true if its a word ending. Otherwise, false will be returned.
        /// </summary>
        /// <value>True will be returned if the node is a word ending. Otherwise, false will be returned.</value>
        public bool IsWordEnding { get; set; }

        /// <summary>
        /// Start index - if the node is a word ending. null will be used for any node that is not a word ending.
        /// </summary>
        /// <value></value>
        public int? StartIndex { get; set; }

        /// <summary>
        /// Checks if the node is a leaf node - with no child
        /// </summary>
        /// <returns>True is returned if the node is a leaf node. Otherwise, false will be returned</returns>
        public bool IsLeaf => !Children.Any(); // [FIXME] - WHAT IF Children is NULL. HMMMM, do something here...

        /// <summary>
        /// Checks if the node have one or more children
        /// </summary>
        /// <returns>True if the node has children. Otherwise, false will be returned.</returns>
        public bool HasChildren => Children != null &&
                           Children.Any();

        /// <summary>
        /// Instantiates a new instance of Node.
        /// </summary>
        public TrieNode()
        {
            Children = new Dictionary<char, TrieNode>();
            StartIndex = null;
            IsWordEnding = false;
        }

        public TrieNode(char character):this()
        {
            Character=character;
        }

        public TrieNode(char character, int? startIndex, bool isWordEnding) : this(character)
        {
            StartIndex = startIndex;
            IsWordEnding = isWordEnding;
        }
    }
}

```

*Step 2* :

**Represent the *Trie* (the whole thing) itself.**

Now, once you have a model representing a single node, modeling the entire Trie data structure is much easier - relativly speaking i.e

As you can see on Figure 4 above, you can think of the whole tree as having a single entry point (*the root node*) - you can call it *root* to speak its intent and what it stands for.
Just notice, the root has no character in it, its simply, a blank node poining to zero or more child nodes.

The *APIs* we would like to expose will include the following. More will be added through time.

- *CreateSuffixTrieFor()* - for a given text T
- *IsSubstring()* - given a pattern, we would like to see if the pattern is a substring of text T
- *SubstringCount()* - given a pattern, we would like to get how many times it appears in text T.
- *Remove()* - given a pattern, remove it from Trie (WIP)
- *FindLongestRepeatedSubstring()* - finds the longest repeated susbstring in the text.
- And more...

```cs

using System;
using System.Collections.Generic;
using System.Linq;
using Common.Extensions;

namespace DS
{
    /// <summary>
    /// Represents Suffix Trie data structure
    /// </summary>
    public class Trie
    {
        /// <summary>
        /// Reference to the root node
        /// </summary>
        private static TrieNode _root;

        /// <summary>
        /// Returns true if the Trie data structure is empty. Otherwise, false will be returned.
        /// </summary>
        /// <returns>True if the Trie data structure is empty. Otherwise, false will be returned.</returns>
        public bool IsEmpty => _root.IsNull() ||
                    _root.Children.IsNull() ||
                    !_root.Children.Any();

        /// <summary>
        /// Creates a new instance of Trie
        /// </summary>
        public Trie()
        {
            _root = new TrieNode();
        }

        /// <summary>
        /// Creates a suffix Trie data structure for a given text
        /// </summary>
        /// <param name="str">text where you would like to build a suffix Trie for.</param>
        public void CreateSuffixTrieFor(string str)
        {
            if(str.IsNull())
            {
                throw new ArgumentNullException($"{nameof(str)} cannot be null.");
            }

            var suffices = str.GetAllSuffices();

            for (int suffixStartIndex = 0; suffixStartIndex < suffices.Count(); suffixStartIndex++)
            {
                Insert(suffices.ElementAt(suffixStartIndex), suffixStartIndex);
            }
        }

        /// <summary>
        /// Inserts a sequence of character of the given word - i.e, sequence of characters into a Trie
        /// </summary>
        /// <param name="word">text or sequence of characters to be added into the Trie data structure.</param>
        /// <param name="index">start index of the word in the main text.</param>
        private void Insert(string word, int index)
        {
            //make sure the root reference isn't null.
            if (_root.IsNull())
            {
                _root = new TrieNode();
            }

            TrieNode current = _root;

            for (int i = 0; i < word.Length; i++)
            {
                current.Children = current.HasChildren
                                ? current.Children
                                : new Dictionary<char, TrieNode>();

                if (current.Children.ContainsKey(word[i]))
                {
                    current = current.Children[word[i]];
                }
                else
                {
                    var isLastIndex = i == word.Length - 1;
                    var startIndex = isLastIndex ? index : (int?)null;
                    var newNode = new TrieNode(character:word[i], startIndex:startIndex, isWordEnding: isLastIndex);
                    current.Children[word[i]] = newNode;
                    current = newNode;
                }
            }

            // set word ending here for word that every character was found in existing Trie.
            current.IsWordEnding = true;
            current.StartIndex = index;
        }

        /*
        TODO: Implement the following
        1. Find out if a string s is substring of T (DONE)
        2. Find out if a string s is suffix of T  (DONE)
        3. Find out the longest repeated substring text of T(WIP)
        4. Find out indexes of substring in T (WIP)
        5. Append $ to the Trie - on a second thought, use flag instead.
        */

        public bool IsSubstring(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                throw new ArgumentNullException($"{nameof(word)} cannot be null or empty.");
            }

            if (IsEmpty)
            {
                return false;
            }

            TrieNode current = _root;

            for (int i = 0; i < word.Length; i++)
            {
                if (!current.Children.ContainsKey(word[i]))
                {
                    return false;
                }

                current = current.Children[word[i]];
            }

            return true ;
        }

        public int SubstringCount(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                throw new ArgumentNullException($"{nameof(word)} cannot be null or empty.");
            }

            if (IsEmpty)
            {
                return 0;
            }

            int count = 0;

            TrieNode current = _root;

            for (int i = 0; i < word.Length; i++)
            {
                if (!current.Children.ContainsKey(word[i]))
                {
                    return 0;
                }

                current = current.Children[word[i]];
            }

            var stack = new Stack<TrieNode>();
            stack.Push(current);

            while(stack.Count >0)
            {
                var topNode = stack.Pop();

                foreach (var childNode in topNode.Children)
                {
                    stack.Push(childNode.Value);
                }

                if(topNode.IsWordEnding)
                {
                    count++;
                }
            }

            return count;
        }
    }
}

```

## Sample Usage of Suffix Trie

```cs
string text = "abaaba";
Trie trie=new Trie();

var defaultForecolor = Console.ForegroundColor;
Console.ForegroundColor = ConsoleColor.Green;
var blueTextColor = ConsoleColor

Console.WriteLine($"Build a trie Data Structure for text: \"{text}\"");

trie.CreateSuffixTrieFor(text);

Console.ForegroundColor = defaultForecolor;

var isAbSub = trie.IsSubstring("ab"); // expect true
var abSubCount = trie.SubstringCount("ab"); // expect 2
var aSubCount = trie.SubstringCount("a"); // expect 4
var aaSubCount = trie.SubstringCount("aa"); // expect 1
var baabSubCount = trie.SubstringCount("baab"); // expect 1
var zabSubCount = trie.SubstringCount("zab"); // expect 0

Console.WriteLine($"Is \"ab\" a substring of \"{text}\": {isAbSub}. Expecting: true");

Console.WriteLine($"Substring count of \"ab\" in text \"{text}\" is: {abSubCount}. Expecting 2.");

Console.WriteLine($"Substring count of \"a\" in text \"{text}\" is: {aSubCount}. Expecting 4");

Console.WriteLine($"Substring count of \"aa\" in text \"{text}\" is: {aaSubCount}. Expecting 1");

Console.WriteLine($"Substring count of \"baab\" in text \"{text}\" is: {baabSubCount}. Expecting 1");

Console.WriteLine($"Substring count of \"zab\" in text \"{text}\" is : {zabSubCount}. Expecting 0");




```

![Sample usage output](https://i.postimg.cc/KzTZhZsS/Sample-Usage.png)

Figure: 5. Sample Usage output.

## Sample Pascal Triangle Generator

Given a number ***n***, its pascal we can generate the pascal triangle as follows:

```cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algorithm
{
    public class PascalTriangleGenerator
    {
        public static List<List<int>> GeneratePascalFor(int n)
        {
            if (n < 1)
            {
                throw new InvalidOperationException($"{nameof(n)} cannot be less than 1.");
            }

            var pascals = new List<List<int>>();

            for (int k = 1; k <= n; k++)
            {
                if (k == 1)
                {
                    pascals.Add(new List<int> { 1 });
                    continue;
                }
                else if (k == 2)
                {
                    pascals.Add(new List<int> { 1, 1 });
                    continue;
                }

                var lastRow = pascals[pascals.Count - 1];
                var newRow = new List<int>() { 1 };

                for (int i = 1; i < lastRow.Count; i++)
                {
                    newRow.Add(lastRow[i - 1] + lastRow[i]);
                }

                newRow.Add(1);

                pascals.Add(newRow);
            }

            return pascals;
        }

        public static string CreatePascalDisplayFormatFor(List<List<int>> pascals)
        {
            var rows = new StringBuilder();

            //for every row in the pascal
            for (int i = 0; i < pascals.Count; i++)
            {
                var row = new StringBuilder();

                //form a row for display
                rows.Append(Environment.NewLine);

                // represent the list of empty spaces before starting to display numbers
                for (int j = 0; j <= pascals.Last().Count - i; j++)
                {
                    row.Append($" ");
                }

                //enclose number inside [ and ] brackets
                row.Append("[");

                // separate numbers on the row with comma
                for (int k = 0; k < pascals[i].Count; k++)
                {
                    row.Append($",{pascals[i][k]}");
                }

                // enclose the numbers on the row with closing ] 
                row.Append("]");

                // remove the first comma that was appeneded above
                int firstIndexOfComma = row.ToString().IndexOf(',');
                row = row.Remove(firstIndexOfComma, 1);

                //add the row to rows
                rows.Append(row);
            }

            //finaly, return rows in displayed format
            var newLine = Environment.NewLine.ToString();
            var firstIndexOfNewLine = rows.ToString().IndexOf(newLine);
            return rows.Replace(newLine, "", firstIndexOfNewLine, newLine.Length).ToString();
        }
    }
}

```

Let's now take a look how to use it. In your client application, example: Main.cs for a console app, lets generate and output for the first few numbers.

```cs
            int n = 7;
            //test for the first 7 numbers.
            for (int i = 1; i <= n; i++)
            {
                var pascals = PascalTriangleGenerator.GeneratePascalFor(i);
                var displayFormat = PascalTriangleGenerator.CreatePascalDisplayFormatFor(pascals);
                Console.WriteLine($"Input: {i}");
                Console.WriteLine($"Output: [{Environment.NewLine}{displayFormat}{Environment.NewLine}]");
                Console.WriteLine("---------------------------------------------");
            }

```

Output for this will look like as follows.

![test-pascal-triangle.png](https://i.postimg.cc/GhbQmY5K/test-pascal-triangle.png)