using System;
using System.Collections.Generic;

namespace Common.Extensions
{
    public static class StringExtensions
    {
           public static IEnumerable<string> GetAllSuffices(this string str)
        {
            if(str.IsNull())
            {
                throw new ArgumentNullException($"{nameof(str)} cannot be null.");
            }

            var suffices = new List<string>();

            for (int i = 0; i < str.Length; i++)
            {
                suffices.Add(str.Substring(i));
            }

            return suffices;
        }
    }
}