﻿using System;
using Newtonsoft.Json;

namespace Common.Extensions
{
    public static class ObjectExtensions
    {
        public static bool IsNull(this object obj)
        {
            return null == obj;
        }

        public static string Jsonify(this object obj)
        {
            if(obj.IsNull())
            {
                throw new ArgumentNullException($"{nameof(obj)} cannot be null.");
            }

            return JsonConvert.SerializeObject(obj, Formatting.Indented);
        }
    }
}
