﻿using Algorithm;
using DS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 7;
            //test for the first 7 numbers.
            for (int i = 1; i <= n; i++)
            {
                var pascals = PascalTriangleGenerator.GeneratePascalFor(i);
                var displayFormat = PascalTriangleGenerator.CreatePascalDisplayFormatFor(pascals);
                Console.WriteLine($"Input: {i}");
                Console.WriteLine($"Output: [{Environment.NewLine}{displayFormat}{Environment.NewLine}]");
                Console.WriteLine("---------------------------------------------");
            }


            string text = "abaaba";
            Trie trie = new Trie();

            var defaultForecolor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Build a trie Data Structure for text: \"{text}\"");
            trie.CreateSuffixTrieFor(text);
            Console.ForegroundColor = defaultForecolor;

            var isAbSub = trie.IsSubstring("ab"); // expect true
            var abSubCount = trie.SubstringCount("ab"); // expect 2
            var aSubCount = trie.SubstringCount("a"); // expect 4
            var aaSubCount = trie.SubstringCount("aa"); // expect 1
            var baabSubCount = trie.SubstringCount("baab"); // expect 1
            var zabSubCount = trie.SubstringCount("zab"); // expect 0

            Console.WriteLine($"Is \"ab\" a substring of \"{text}\": {isAbSub}. Expecting: true");
            Console.WriteLine($"Substring count of \"ab\" in text \"{text}\" is: {abSubCount}. Expecting 2.");
            Console.WriteLine($"Substring count of \"a\" in text \"{text}\" is: {aSubCount}. Expecting 4");
            Console.WriteLine($"Substring count of \"aa\" in text \"{text}\" is: {aaSubCount}. Expecting 1");
            Console.WriteLine($"Substring count of \"baab\" in text \"{text}\" is: {baabSubCount}. Expecting 1");
            Console.WriteLine($"Substring count of \"zab\" in text \"{text}\" is : {zabSubCount}. Expecting 0");


            Console.ReadLine();

            StringBuilder sb = new StringBuilder();

            int[] numbers = { 8, 3, 10, 1, 6, 14, 4, 7, 13 };
            DS.BinarySearchTree<int> bst = new DS.BinarySearchTree<int>();

            for (int i = 0; i < numbers.Length; i++)
            {
                bst.Insert(numbers[i]);
                sb.Append($", {numbers[i]}");
            }

            Console.WriteLine("....... Numbers used to construct the Binary Search Tree ............");
            Console.WriteLine(sb.ToString().Remove(0, 2));
            sb.Clear();


            var preOrderTraversal = bst.PreOrder();

            foreach (var num in preOrderTraversal)
            {
                sb.Append($", {num}");
            }

            Console.WriteLine(".............PreOrder Traversal.................");
            Console.WriteLine(sb.ToString().Remove(0, 2));
            Console.WriteLine();
            sb.Clear();

            var inOrderTraversal = bst.InOrder();

            foreach (var num in inOrderTraversal)
            {
                sb.Append($", {num}");
            }

            Console.WriteLine("............InOrder Traversal..................");
            Console.WriteLine(sb.ToString().Remove(0, 2));
            Console.WriteLine();
            sb.Clear();


            var postOrderTraversal = bst.PostOrder();

            foreach (var num in postOrderTraversal)
            {
                sb.Append($", {num}");
            }

            Console.WriteLine(".............PostOrder Traversal.................");
            Console.WriteLine(sb.ToString().Remove(0, 2));
            Console.WriteLine();
            sb.Clear();
            // BubbleSort<int> bubbleSorter = new BubbleSort<int>();

            // int[] numbers = { 9, 6, 4, 3, 1, 6, 8, 6, 3, 2, 12 };

            // bubbleSorter.Sort(numbers);

            // for (int i = 0; i < numbers.Length; i++)
            // {
            //     Console.WriteLine(numbers[i]);
            // }

            Console.ReadLine();
            // DS.LinkedList<int> list = new DS.LinkedList<int>();

            // for (int i = 1; i < 6; i++)
            // {
            //     list.AddToHead(i);
            // }


            // DS.Stack<int> stack = new DS.Stack<int>();
            // for(int i=1; i<= 10;i++)
            // {
            //     stack.Push(i);
            // }

            // foreach(var x in stack)
            // {
            //     Console.WriteLine(x);
            // }

            // Console.WriteLine($"3 should be contained: {stack.Contains(3)}");

            // Console.WriteLine($"33 should not be contained: {stack.Contains(33)}");



            Console.WriteLine("Press [Enter] to exit.");
            Console.ReadLine();
        }
    }
}
