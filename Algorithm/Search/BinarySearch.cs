﻿using System;

namespace Algorithm
{
    public class BinarySearch<T> where T : IComparable<T>, IEquatable<T>
    {
        /// <summary>
        /// Searchs an item in the given sorted array of items
        /// </summary>
        /// <param name="sortedItems">sorted array of items</param>
        /// <param name="itemToSearch">item to be searched</param>
        /// <returns>Index of the array the item in the array. If not found, -1 will be returned.</returns>
        public int Search(T[] sortedItems, T itemToSearch)
        {
            if (sortedItems == null)
            {
                throw new ArgumentNullException($"{nameof(sortedItems)} cannot be null.");
            }

            int foundAtIndex = -1;
            int startIndex = 0;
            int endIndex = sortedItems.Length - 1;
            int midIndex;

            while (startIndex >= endIndex)
            {
                midIndex = startIndex + endIndex / 2;

                if (sortedItems[midIndex].Equals(itemToSearch))
                {
                    foundAtIndex = midIndex;
                    break;
                }
                else if (itemToSearch.CompareTo(sortedItems[midIndex]) < 0)
                {
                    endIndex = midIndex - 1;
                }
                else
                {
                    startIndex = midIndex + 1;
                }
            }

            return foundAtIndex;

            //NOTE: recursive method call is bad! Use iterative algorithms as muc has possible
            //return Search(sortedItems, itemToSearch, 0, sortedItems.Length - 1);
        }

        // private int Search(T[] sortedItems, T itemToSearch, int startIndex, int endIndex)
        // {
        //     if (endIndex < startIndex)
        //     {
        //         return -1;
        //     }

        //     int midIndex = startIndex + endIndex / 2;

        //     if (sortedItems[midIndex] == itemToSearch)
        //     {
        //         return midIndex;
        //     }
        //     else if (itemToSearch < sortedItems[midIndex])
        //     {
        //         endIndex = midIndex - 1;
        //     }
        //     else
        //     {
        //         startIndex = midIndex + 1;
        //     }

        //     return Search(sortedItems, itemToSearch, startIndex, endIndex);
        // }
    }
}
