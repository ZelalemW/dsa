﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algorithm
{
    public class PascalTriangleGenerator
    {
        public static List<List<int>> GeneratePascalFor(int n)
        {
            if (n < 1)
            {
                throw new InvalidOperationException($"{nameof(n)} cannot be less than 1.");
            }

            var pascals = new List<List<int>>();

            for (int k = 1; k <= n; k++)
            {
                if (k == 1)
                {
                    pascals.Add(new List<int> { 1 });
                    continue;
                }
                else if (k == 2)
                {
                    pascals.Add(new List<int> { 1, 1 });
                    continue;
                }

                var lastRow = pascals[pascals.Count - 1];
                var newRow = new List<int>() { 1 };

                for (int i = 1; i < lastRow.Count; i++)
                {
                    newRow.Add(lastRow[i - 1] + lastRow[i]);
                }

                newRow.Add(1);

                pascals.Add(newRow);
            }

            return pascals;
        }

        public static string CreatePascalDisplayFormatFor(List<List<int>> pascals)
        {
            var rows = new StringBuilder();

            //for every row in the pascal
            for (int i = 0; i < pascals.Count; i++)
            {
                var row = new StringBuilder();

                //form a row for display
                rows.Append(Environment.NewLine);

                // represent the list of empty spaces before starting to display numbers
                for (int j = 0; j <= pascals.Last().Count - i; j++)
                {
                    row.Append($" ");
                }

                //enclose number inside [ and ] brackets
                row.Append("[");

                // separate numbers on the row with comma
                for (int k = 0; k < pascals[i].Count; k++)
                {
                    row.Append($",{pascals[i][k]}");
                }

                // enclose the numbers on the row with closing ] 
                row.Append("]");

                // remove the first comma that was appeneded above
                int firstIndexOfComma = row.ToString().IndexOf(',');
                row = row.Remove(firstIndexOfComma, 1);

                //add the row to rows
                rows.Append(row);
            }

            //finaly, return rows in displayed format
            var newLine = Environment.NewLine.ToString();
            var firstIndexOfNewLine = rows.ToString().IndexOf(newLine);
            return rows.Replace(newLine, "", firstIndexOfNewLine, newLine.Length).ToString();
        }
    }
}
