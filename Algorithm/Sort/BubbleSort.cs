using System;

namespace Algorithm
{
    public class BubbleSort<T> where T : IComparable<T>
    {
        public void Sort(T[] items)
        {
            if (items == null)
            {
                throw new ArgumentNullException($"{nameof(items)} cannot be null.");
            }

            // for every item from first to the second from last, compare them and swap as needed to keep them 
            // in sorted order.
            for (int i = 0; i < items.Length; i++)
            {
                for (int j = i + 1; j < items.Length; j++)
                {
                    // if not in their correct order, swap them
                    if (items[i].CompareTo(items[j]) > 0)
                    {
                        Swap(items, i, j);
                    }
                }
            }
        }

        /// <summary>
        /// Swaps two items 
        /// </summary>
        /// <param name="items">collection from which two items will be switched</param>
        /// <param name="i">index of the first item to be switched</param>
        /// <param name="j">index of the second item to be switched</param>
        private void Swap(T[] items, int i, int j)
        {
            var temp = items[i];
            items[i] = items[j];
            items[j] = temp;
        }
    }
}