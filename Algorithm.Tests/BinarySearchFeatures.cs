using System;
using Xunit;
using Algorithm;
using Xbehave;

namespace Algorithm.Tests
{
    public class BinarySearchFeatures
    {
        private BinarySearch<int> _binarySearch;

        public BinarySearchFeatures()
        {
            _binarySearch = new BinarySearch<int>();
        }

        [Fact]
        public void Binary_search_should_return_correct_index_when_item_exists()
        {
            int[] sortedNumbers = { 1, 2, 3, 4, 5, 6 };
            int existingNumber = sortedNumbers[3];
            int index = -1;

            "Given a binary search"
              .x(() => _binarySearch = new BinarySearch<int>());
            "And sorted array of Integers"
              .x(() => sortedNumbers = new int[] { 1, 2, 3, 4, 5, 6 });
            "And existing item to search for"
              .x(() => existingNumber = 4);
            "When Sarching the item"
              .x(() => index = _binarySearch.Search(sortedNumbers, existingNumber));
            "Then correct index is returned."
              .x(() => Assert.True(3 == index));
        }

        [Fact]
        public void Binary_search_should_return_negative_1_for_non_existing_item()
        {
            int[] sortedNumbers = { 1, 2, 3, 4, 5, 6 };
            int nonExistingNumber = 100;
            int index = -1;

            "Given a binary search"
              .x(() => _binarySearch = new BinarySearch<int>());
            "And sorted array of Integers"
              .x(() => sortedNumbers = new int[] { 1, 2, 3, 4, 5, 6 });
            "And non-existing item to search for"
              .x(() => nonExistingNumber = 100);
            "When Sarching an item"
              .x(() => index = _binarySearch.Search(sortedNumbers, nonExistingNumber));
            "Then -1 is returned."
              .x(() => Assert.True(-1 == index));
        }

        [Fact]
        public void Binary_search_should_throw_exception_when_array_is_null()
        {
            int[] sortedItems = null;
            int itemToSearch = 100;
            Exception ex = null;

            "Given a binary search"
              .x(() => _binarySearch = new BinarySearch<int>());
            "And [null] sorted array of Integers"
              .x(() => sortedItems = null);
            "And a number to search"
              .x(() => itemToSearch = 100);
            "When Sarching an item"
              .x(() => ex = Assert.Throws<ArgumentNullException>(() => _binarySearch.Search(sortedItems, itemToSearch)));
            "Then ArgumentNullException should be thrown."
              .x(() => Assert.Equal("sortedItems cannot be null.", ex.Message));
        }
    }
}
