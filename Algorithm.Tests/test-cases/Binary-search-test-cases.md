# Binary Search Test Cases

**Binary_search_should_return_correct_index_when_item_exists:**

- Given a binary search
- And sorted array of Integers
- And existing item to search for
- When Sarching the item
- Then correct index is returned.

**Binary_search_should_return_-1_for_non_existing_item:**

- Given a binary search
- And sorted array of Integers
- And non-existing item to search for
- When Sarching an item
- Then -1 is returned.

**Binary_search_should_throw_exception_when_array_is_null:**

- Given a binary search
- And [null] sorted array of Integers
- And a number to search
- When Sarching an item
- Then ArgumentNullException should be thrown.
